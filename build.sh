#!/bin/sh
MUVIRT_BUILD_TARGET=$1
if [ ! -f "configs/${MUVIRT_BUILD_TARGET}" ]; then
	echo "Target must be supplied"
	echo "Currently defined targets are: "
	ls configs/
	exit 1
fi
set -e

cat > muvirt-lede/feeds.conf <<EOF
src-link muvirt $(readlink -f muvirt-feed)
src-git packages https://git.lede-project.org/feed/packages.git
src-git luci https://git.lede-project.org/project/luci.git
src-git openwisp https://github.com/openwisp/openwisp-config.git
EOF

cp "configs/${MUVIRT_BUILD_TARGET}" muvirt-lede/wrt-config
DOWNLOAD_DIR=$(readlink -f downloads)
echo "CONFIG_DOWNLOAD_FOLDER=\"$DOWNLOAD_DIR\"" >> muvirt-lede/wrt-config

cd muvirt-lede
./scripts/feeds clean
./scripts/feeds update -a
./scripts/feeds install luci
./scripts/feeds install dosfstools
./scripts/feeds install muvirt
./scripts/feeds install netkit-telnet
./scripts/feeds install lvm2
./scripts/feeds install tmux
./scripts/feeds install ethtool
./scripts/feeds install ncat
./scripts/feeds install rng-tools
./scripts/feeds install pciutils
./scripts/feeds install -p openwisp openwisp-config-openssl
./scripts/feeds install -p muvirt qemu-nbd
./scripts/feeds install -p muvirt qemu-img
./scripts/feeds install -p muvirt eglibc-locale-en-utf8
./scripts/feeds install -p muvirt parted
# Do a defconfig on a fresh tree right away, otherwise
# packages requiring deps from feeds will drop out in our
# config
make defconfig
cp wrt-config .config
make defconfig

grep "CONFIG_PACKAGE_muvirt=y" .config || (echo "muvirt not selected" && exit 1)
grep "CONFIG_PACKAGE_luci=y" .config || (echo "LuCI not selected" && exit 1)
grep "CONFIG_PACKAGE_lvm2=y" .config || (echo "LVM not selected" && exit 1)
grep "CONFIG_PACKAGE_tmux=y" .config || (echo "tmux not selected" && exit 1)
grep "CONFIG_PACKAGE_qemu-img" .config || (echo "qemu-img not selected" && exit 1)
grep "CONFIG_PACKAGE_qemu-nbd" .config || (echo "qemu-nbd not selected" && exit 1)

echo "Building with `nproc` cores"
make -j`nproc`
