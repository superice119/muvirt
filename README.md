# μVirt - really small virtualisation

See the [Traverse μVirt page](https://traverse.com.au/application-library/muvirt/) for a demonstration video and introduction.

μVirt (aka "micro"Virt or muVirt) is a small virtualisation host built on top of OpenWRT/LEDE, primarily
designed to host simple virtual network functions (VNF) on [universal CPE (uCPE)](https://www.sdxcentral.com/articles/contributed/understanding-use-universal-cpe/2017/07/)
boxes.

The current iteration works on 64-bit ARM hardware and virtual machines compliant
with the [Linaro VM System Specification](https://www.linaro.org/blog/vm-system-specification-v2-arm-processors/) (i.e VM images that use UEFI to boot)

## What μVirt is
- Small - the system image fits inside the NAND or QSPI flash (<64MB), allowing bulk storage (NVMe Flash, SATA and/or RAID) to be dedicated to virtual machines
- Integrates with OpenWRT UCI, configuration in /etc/config/virt, VMs bound to OpenWRT bridge network, can use OpenWRT services such as DHCP or Firewall.


## What μVirt is not
- A replacement for OpenStack, oVirt, Proxmox or vSphere

Think ESXi vs vSphere, HyperV vs SCCM. For example, μVirt hosts have local storage only.

- Production ready - this is currently a tech demonstration. Use at own risk.

- Only serial consoles to the VM are supported, no VNC/SPICE support.

VMs compliant to the Linaro VM specification should have working consoles on ttyAMA0 (as do 'real' ARM servers/those complying with SBAS).

## Current supported hardware:
- Traverse LS1043 Family

In theory, any virtualisation capable Aarch64 OpenWRT host should work, this means any CPU with GICv2 or later interrupt controller.
Use on other KVM-enabled platforms (x86, POWER) is possible but not planned at this stage.

At this time, only 64-bit hosts and VMs are supported (no Aarch32 support).

## OpenWISP Integration
We are experimenting various management systems, including OpenWISP 2.
See [this repository](https://github.com/mcbridematt/netjsonconfig/tree/muvirt-support-public) for the latest work.

## TODO:
- Backup methods
- OpenvSwitch and OpenvSwitch-DPDK integration
- Examine if using libvirt is worth it (we currently execute qemu-system-aarch64 directly)
- Examine methods for cloud-init configuration (9p, DHCP)
- Implement friendlier methods of VM shutdown and reset (```/etc/init.d/muvirt.<vnmame>``` stop causes a hard shutdown)

## Build notes:
This repository has two submodules (muvirt-lede and muvirt-feed), to obtain the μVirt sources do a
recursive clone:
```
git clone --recursive https://gitlab.com/traversetech/muvirt/
```

To build for the traverse-ls1043 target:
```
./build.sh traverse-ls1043
```

## Configuration example:
It is a good idea to set 'force_link' on all networks you intend to use with VM's, otherwise the bridge network might only come up when there is a physical connection, causing the VM to have an unconnected network interface.
```
cat /etc/config/network
	config network 'lan'
		option force_link '1'
		option type 'bridige'
		...
```
If you are using the 'lan' interface, and muvirt is not intended as the router for the local LAN, be sure to
disable dnsmasq:
```
/etc/init.d/dnsmasq disable
```

Here, LVM is used as the storage backend, and a LEDE VM is bridged to two outside interfaces.
```
# cat /etc/config/virt
config disk 'lededisk'
        option path '/dev/vg1/lvol0'
        option type 'virtio-blk'

config vm 'lede'
        option type 'aarch64'
        option cpu 'host'
        option memory 128
        option numprocs 1
        option machine 'virt,gic_version=2'
        list mac '52:54:00:F4:A2:BD 52:54:00:CA:5A:A4'
        list disks 'lededisk'
        list network 'privatelan privatewan'
        option enable 1

# cat /etc/config/network
config interface 'privatelan'
        option type 'bridge'
        option ifname 'eth2'
        option proto 'none'

config interface 'privatewan'
        option type 'bridge'
        option ifname 'eth3'
        option proto 'none'
```

### RNG/Random host configuration
A virtio-rng device is supplied to the VM, on some machines this may require the host RNG to be configured properly, e.g using ```rng-tools```.
If your VM boots stall early in the boot process (just after GRUB, for example), this is most likely due to lack of entrophy in the host's RNG. (Check ```/proc/sys/kernel/random/entropy_avail```)

## Adding a VM
TODO: Add a `muvirt-deploy` command 
```
lvcreate -L 20GiB --name <vmname> vg1
dd if=vmname.img of=/dev/vg1/<vmname> # or qemu-img convert -O vmname.qcow2 /dev/vg1/<vmname>
vi /etc/config/virt
ln -s /etc/init.d/muvirt /etc/init.d/muvirt.<vmname>
/etc/init.d/muvirt.<vmname> enable
/etc/init.d/muvirt.<vmname> start
```

On boot, /etc/init.d/muvirt will create symlinks for any VMs that are enabled (option enable 1). This means VMs will be set up correctly after a sysupgrade.

## Accessing VM consoles
Use `muvirt-console <vmname>` to spawn a tmux session to the child VM
Use the tmux disconnect sequence ( Ctrl-B ) to leave the session

_NOTE_: Do not attempt to use muvirt-console over the host's serial terminal, ```muvirt-console```
uses tmux which doesn't like /dev/console.

## Using USB Devices
Passthrough of USB devices is supported, this is useful for applications that require external hardware (e.g radios)

The syntax is the same as QEMU's ```-device usb-host,...```,for example, to pass through USB host bus 1 to the VM:
```
	config vm 'usbpassthrough'
		list usbdevice 'hostbus=1'
```

For references on the QEMU USB passthrough feature, see [linux-kvm.org](https://www.linux-kvm.org/page/USB_Host_Device_Assigned_to_Guest) and 
By default, a USB 3 (XHCI) controller is specified. If your board only has USB2 support, you will need to set ```virt.system.usbcontroller``` to 'usb-ehci'

### A note on passing through host UARTs (for IoT, uC/Arduino's etc.)
Unfortunately the ```virt``` machine model for ARM64 in QEMU only supports one serial port (used for the ttyAMA0) console so the passthrough of host 
TTYs is not [currently possible](https://unix.stackexchange.com/questions/479085/can-qemu-m-virt-on-arm-aarch64-have-multiple-serial-ttys-like-such-as-pl011-t). 
We have tried to use emulated 8250 devices under QEMU but these do not pass flow control signals, so may not be useful.
If your application depends on controlling outside things via UART, I2C or SPI, consider having a small host side proxy to do the hardware interfacing.
USB-serial controllers (e.g FTDI) will work with the USB passthrough functionality described above.

## Where to get virtual machine images
- Alpine Linux
    * [Alpine Image Build Script](https://gitlab.com/mcbridematt/build-alpine-aarch64-efi/). This customizable image is useful as a low-overhead Docker/container host.
- Ubuntu
    * [Ubuntu Cloud Images](http://cloud-images.ubuntu.com), such as zesty-server-cloudimage-arm64. These use cloud-init by default.
- Fedora
    * [Traverse Fedora Image Build Script](https://gitlab.com/matt_traverse/build-fedora-aarch64-efi)
    * [Fedora Alternative Architectures](https://alt.fedoraproject.org/alt/)
- Debian
    * [Traverse Debian Image Build Script](https://gitlab.com/matt_traverse/build-debian-aarch64-efi)
- OpenWRT
	* [Traverse OpenWRT ARMv8-UEFI tree](https://gitlab.com/muvirt/openwrt-armvm-uefi) (upstream pending)
- FreeBSD
	* [FreeBSD 11.1](http://ftp.freebsd.org/pub/FreeBSD/releases/VM-IMAGES/11.1-RELEASE/aarch64/Latest/)
	(requires an older QEMU_EFI.fd, see [https://wiki.freebsd.org/arm64/QEMU](FreeBSD Wiki))
- OpenBSD
	* [Notes on OpenBSD](https://gitlab.com/traversetech/muvirt/wikis/OpenBSD)
	(like FreeBSD, requires an older QEMU_EFI.fd. Currently not stable under QEMU)

## Acknowledgements
Many thanks to the following people for their contributions:
	* Thomas Niederprüm contributed support for USB passthrough, memory ballooning, virtio-rng and enhancements to the Alpine image builder.
